package main

import (
	"fmt"
	"io"
	"os"

	"github.com/dim13/cobs"
)

func main() {
	encode := true
	for _, arg := range os.Args[1:] {
		switch arg {
		case "-e":
			encode = true
		case "-d":
			encode = false
		}
	}

	for {
		buf := make([]byte, 255+2)
		n, err := os.Stdin.Read(buf)
		if err != nil {
			if err != io.EOF {
				fmt.Fprintf(os.Stderr, "Cannot read data: %s\n", err.Error())
			}
			return
		}

		var c []byte
		if encode {
			c = cobs.Encode(buf[0:n])
		} else {
			c = cobs.Decode(buf[0:n])
		}
		_, err = os.Stdout.Write(c)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Write error: %s\n", err.Error())
		}
	}
}

# COBS command line utility

This is a command line encoder/decoder for COBS.

*COBS* (Consistent Overhead Byte Stuffing) is a method of encoding serial data.

Thanks for https://github.com/dim13/cobs

## How to use

Encode:

```sh
$ echo -n "abcdefg" | cobs > data.bin
$ hexdump -C data.bin
00000000  08 61 62 63 64 65 66 67  00                       |.abcdefg.|
00000009

$ echo 00 | xxd -r -p | cobs | hexdump -C
00000000  01 01 00                                          |...|
00000003
```

Decode:

```sh
$ cat data.bin | cobs -d | hexdump -C
00000000  61 62 63 64 65 66 67                              |abcdefg|
00000007

$ echo 01010100 | xxd -r -p | cobs -d | hexdump -C
00000000  00 00                                             |..|
00000002
```
